# Apache2 Reverse Proxy Configure

[![Version](https://img.shields.io/badge/version-2019.08.20-red.svg)](./CHANGELOG)  [![License](https://img.shields.io/github/license/mashape/apistatus.svg)](./LICENSE)

* Wisoft Lab.  Apache Reverse Proxy Setting and Configure 

## Explain

* 연구실 서비스는 apache2의 reverse proxy를 통해서 접속이 가능합니다. 
* 본 문서는 연구실에 셋팅되어 사용되고 있는 reverse proxy 설정 값을 서술하고 있습니다. 
* HTTPS인증을 위해 Free SSL/TLS Certification [Let's Encrypt ](https://letsencrypt.org/)를 사용합니다. 
  * Certbot을 통해 자동 인증 갱신 및 인증서 발급을 진행합니다. 



### Policy

* apache2 설정 파일의 경로는 다음과 같습니다. 
  * /etc/apache2/sites-available

---

* **000-service.conf**
  * 연구실 홈페이지
* **1xx-service.conf**
  * 인프라 서비스 
  * 보안을 위해 교내에서만 접속 가능
* **2xx-service.conf**
  * 모니터링 및 관리 서비스
* **300-service**
  * 연구실 서비스
* **9xx-service.conf**
  * 개인 / 별도 도메인 
* **1000-service.conf**
  * 임시 / 수업과 관련된 서비스



## Exception Handling

다음 HTTP Status Code에 대해서 오류발생 상황을 통보합니다. 관련 오류가 지속적으로 발생할 경우 관리자에게 문의하세요. 

* 400 - Bad Request
* 401 - Unauthorized
* 403 - Forbidden
* 404 - Not Found
* 500 - Internal Server Error
* 501 - Not Implemented
* 502 - Bad Gateway
* 503 - Service Unavailable



### Apache2 conf file enable and disable

* conf 파일을 활성화 하려면 다음과 같이 실행합니다. 

  ```bash
  $ sudo a2ensite ***-***.conf
  $ sudo systemctl reload apache2
  ```

* conf 파일을 비활성화 하려면 다음과 같이 실행합니다. 

  ```bash
  $ sudo a2dissite ***-***.conf
  $ sudo systemctl reload apache2
  ```


### certbot setting 

* 인증서를 등록하기 위해서는 다음과 같이 실행합니다. 

  ```bash
  $ sudo certbot --apache -d <domain>
  ```

  * 인증서만 따로 발급받기를 원한다면 다음과 같이 실행합니다. 

    ```shell
    $ sudo certbot certonly -d <domain>
    ```

    

* 인증서를 삭제하기 위해서는 다음과 같이 실행합니다. 

  ```bash
  $ sudo certbot delete --cert-name <domain>
  ```

* 모든 인증서를 갱신하기 위해서는 다음과 같이 실행합니다. 

  ```bash
  $ sudo certbot renew
  ```


## Authors

- **Seongwon LEE** 
